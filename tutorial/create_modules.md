<div style="text-align: center">
  <img alt="discord_bot_typescript_base" src="https://i.ibb.co/pQMZdL6/Bild-2023-05-18-101017142.png" width="300" align="center">
  <h1>
    Discord Bot - TypeScript Boilerplate    
  </h1>
  <h3>
    DiscordJS - MySQL - MongoDB
  </h3>
</div>

<h1 style="color: cornflowerblue">
  Own Modules Folder
</h1>

To create you own modules you should create a folder `plugins` in the `./src/` folder.
The boilerplate will also import all files from `events` folders which are in this folder.<br>
The plugins folder is also a way to fix issues which are coming up when you're creating new functions to easily find out where the problem comes from to undo changes.<br>
<br>
You can create a structure like
```
/src
├── /plugins
│   ├── /events
│   ├── /scripts
│   ├── index.ts
```
or
```
/src
├── /plugins
│   ├── /plugin_one
│   │   ├── /events
│   │   ├── index.ts
│   ├── index.ts
```
all files in `events` folders will be imported.

<h1 style="color: cornflowerblue">
  Use Core From The Boilerplate
</h1>

The boilerplate itself has a public variable `Core` which you can import in every file also in your plugins.<br>
This variable has all services and modules in it. To use your variables just follow the steps below.

<h3 style="color: orange">
  Own Variables In Core
</h3>

Create a file `index.ts` with this content in the `/plugins` folder:
```ts
import { Core } from "../startup.js";

class PluginClass {
  public init(): void {
    //
  }
}

export const Plugins: PluginClass = new PluginClass();
```
After you have created this file add this line into the `/src/startup.ts`:
```ts
import { Client, Events, GatewayIntentBits, ActivityType } from "discord.js";
import { Modules } from "./modules/index.js";
import { Services } from "./services/index.js";
import { Database } from "./database/index.js";
import { Plugins } from "./plugins/index.js"; // <- import the PluginClass into Core

let Core: CoreClass;

class CoreClass {

  public Modules = Modules;
  public Services = Services;
  public EntityArray: any[] = [];
  public Database = Database;
  public Config: any;
  public Plugins = Plugins; // <- add the PluginClass as variable into Core
```
Now you could use your plugins everywhere in your code.
You only have to import all plugin classes as variables into the `/plugins/index.ts` file.<br>
I recommend to use this simple class and export structure in your files to net get confused.
```ts
class <ClassName>Class {
  public test(): void {} // <- can be used everywhere
  private init(): void {} // <- can be used just in this class
}

export const <VariableName>: <ClassName>Class = new <ClassName>Class();
```
As an example you can now import this variable `<VariableName>` into your `/plugins/index.js` to use it everywhere.
```ts
import { Core } from "../startup.js";
import { <VariableName> } from "./plugin.js";

class PluginClass {
  
  public Variable = <VariableName>; // <- add it inside the class
  
  public init(): void {
    //
  }
}

export const Plugins: PluginClass = new PluginClass();
```
Now you're ready to go and you can use all functions inside the `<ClassName>` class:
```ts
Core.Plugins.Variable.test();
```