<div style="text-align: center">
  <img alt="discord_bot_typescript_base" src="https://i.ibb.co/pQMZdL6/Bild-2023-05-18-101017142.png" width="300" align="center">
  <h1>
    Discord Bot - TypeScript Boilerplate    
  </h1>
  <h3>
    DiscordJS - MySQL - MongoDB
  </h3>
</div>

<h1 style="color: cornflowerblue">
  Boilerplate checklist
</h1>

<span style="color: orange;">✅ create startup.js & index.js</span><br>
<span style="color: orange;">✅ MySQL & MongoDB support</span><br>
<span style="color: orange;">⌛ Test Database Types</span><br>
<span style="color: orange;">⌛ Improve Discord.JS Base (Commands, Functions, ...)</span><br>
<span style="color: orange;">❌ Finish readme.md</span><br>

<h1 style="color: cornflowerblue">
  Getting started
</h1>

To get started with this boilerplate, execute `npm install` in the terminal of your IDE.<br>
All the files are located in the `/src` folder of this boilerplate. When you build the bot it will create a `/dist` folder with all JS files in it.

<h3 style="color: orange">
  Useful terminal commands
</h3>

| command           | description                                                           |
|-------------------|-----------------------------------------------------------------------|
| npm run clean     | cleanup `/dist` folder                                                |
| npm run build     | build your JS files without starting the bot                          |
| npm run start     | build your JS files and start the bot                                 |
| npm run reinstall | delete your `node_modules` folder and install all dependencies again  |

<h1 style="color: cornflowerblue">
  Working with the boilerplate
</h1>

<ol style="list-style-type: none;">
<li>
<details>
<summary style="color: orange; font-weight: bold">Install the boilerplate</summary>

Execute `npm install` to install all needed dependencies.<br>
For databases you can use MySQL or MongoDB. You have just to set the variable **databaseType** in the [index.ts](./src/index.ts) file.<br>

 <details>
  <summary style="color: orange; font-weight: bold">Database config</summary>
  
  As I said before you can choose between MySQL and MongoDB.<br>
  Choose the database that fits for you and copy the config below into your `config.yml` file.

  #### MySQL
  ```yaml
  # MySQL variables
  database: {
      type: 'mysql',
      host: '<IP>',
      port: '<PORT>',
      username: '<USERNAME>',
      password: '<PASSWORD>',
      database: '<DATABASE_NAME>',
    }
  ```
  #### MongoDB
  Credits: [Stuyk/EzMongoDB](https://github.com/Stuyk/EzMongoDB/tree/main)
  <p>
    <span style="color: green">mongodb</span>://
    <span style="color: orange">user:pass</span>@
    <span style="color: dodgerblue">host:port</span>/ ᐅ MONGO_URL
    <br>
    <span style="color: green">protocol</span> &nbsp;-&nbsp;
    <span style="color: orange">credentials</span> &nbsp;-&nbsp;
    <span style="color: dodgerblue">host + port</span>
  </p>

  ```yaml
  # MongoDB variables
  database: {
    url: '<MONGODB_URL>',
    name: '<DATABASE_NAME>'
  }
  ```
  </details>
</details>
</li>
<br>
<li>
<details>
  <summary style="color: orange; font-weight: bold">Initialize your bot</summary>

Go into the `config.yml` file in the root folder and change `<token here>` to your bot token.
  ```yaml
  # Bot config
  Bot:
    token: '<token here>'
    database: '<type>' # 'mysql' or 'mongodb'
    client_id: '<client id>'
    guild_id: '<guild id>'
  ```
</details><br>

<details>
  <summary style="color: orange; font-weight: bold">Create Modules</summary>

  You can find everything in the [Create Modules](./tutorial/create_modules.md) Tutorial about how to work with this boilerplate.
</details><br>

<details>
  <summary style="color: orange; font-weight: bold">Create Your Bot</summary>

  You can find everything in the [Create Your Bot](./tutorial/create_your_bot.md) Tutorial about how to work with this boilerplate.
</details><br>

</li>
</ol>
