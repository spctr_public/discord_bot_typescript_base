import {Db, MongoClient, ObjectId} from 'mongodb';
import {Core} from "../../startup.js";

class MongoDBClass {
  private isInitialized = false;
  private client: MongoClient;
  private db: Db;
  private databaseType: string = 'MongoDB';

  init(url: string, databaseName: string, collections: Array<string>): void {
    try {

      this.client = new MongoClient(url, {retryReads: true, retryWrites: true});

      if (!this.didConnect) {
        Core.Services.Logger.error(this.databaseType, `Failed to connect to Database with ${url}. Double-check specified URL, and ports.`)
        return;
      }

      // Force Reconnection
      this.client.on('connectionClosed', () => {
        this.isInitialized = false;
        Core.Services.Logger.error(this.databaseType, `Failed to connect to Database, retrying connection...`)
        this.init(url, databaseName, collections);
      });

      this.db = this.client.db(databaseName);

      for (let i: number = 0; i < collections.length; i++) {
        const collectionName: string = collections[i];
        const collectionIndex = this.currentCollections(collectionName);
        if (Number(collectionIndex) >= 0) {
          continue;
        }

        this.initCreateCollection(collectionName);
        Core.Services.Logger.database(this.databaseType, `Generated Collection - ${collectionName}`);
      }

      Core.Services.Logger.database(this.databaseType, `Connection Established`);
      this.isInitialized = true;
    } catch (error) {
      Core.Services.Logger.error(this.databaseType, `${error}`);
      return;
    }
  }

  /**
   * This functions are needed to use await in the 'new' case in startup.ts.
   * 'new' cases can't be used in Promise<void> returns.
   */
  private async initCreateCollection(collectionName: string): Promise<void> {
    try {
      await this.db.createCollection(collectionName);
    } catch (error) {
      Core.Services.Logger.error(this.databaseType, `Collections can't be created`);
    }
  }
  private async currentCollections(collectionName: string): Promise<number> {
    try {
      let res = await this.db.collections();
      return res.findIndex((x): boolean => x.collectionName === collectionName);
    } catch (error) {
      Core.Services.Logger.error(this.databaseType, `${error}`)
      return null;
    }
  }
  private async didConnect(): Promise<void> {
    await this.client.connect()
      .then((res) => {
        return true;
      })
      .catch((err) => {
      console.error(err);
      return false;
    });
  }

  /**
   * Used to determine if the database has finished initializing.
   * @static
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async hasInitialized(): Promise<boolean> {
    return new Promise((resolve) => {
      if (this.isInitialized) {
        return resolve(true);
      }

      const timeout = setInterval(() => {
        if (!this.isInitialized) {
          return;
        }

        clearTimeout(timeout);
        return resolve(true);
      }, 250);
    });
  }

  /**
   * Returns the database currently being worked with directly.
   * @returns {Db}
   */
  async getDatabaseInstance(): Promise<Db> {
    await this.hasInitialized();
    return this.db;
  }

  /**
   * Returns if a collection exists.
   * @param {string} collection
   * @returns
   */
  async doesCollectionExist(collection: string): Promise<boolean> {
    await this.hasInitialized();

    const currentCollections = await this.db.collections();

    const index = await currentCollections.findIndex((x) => x.collectionName === collection);
    return index >= 0;
  }

  /**
   * Create a collection if the collection does not exist.
   * @param {string} collection
   * @param {boolean} returnFalseIfExists - Defaults to false, but if set to true returns false if collection exists already
   **/
  async createCollection(collection: string, returnFalseIfExists = false): Promise<boolean> {
    if (!collection || typeof collection !== 'string') {
      console.error(`Failed to specify collections.`);
      return false;
    }

    await this.hasInitialized();

    const currentCollections = await this.db.collections();

    const index = await currentCollections.findIndex((x) => x.collectionName === collection);
    if (index >= 0) {
      return !returnFalseIfExists;
    }

    const result = await this.db.createCollection(collection).catch((err) => {
      return false;
    });

    if (!result) {
      return result as boolean;
    }

    return true;
  }

  /**
   * Find one document by key and value pair. Equivalent of fetching by an id.
   * Use case: Fetching a single document with an id, name, username, etc.
   * @static
   * @template T
   * @param {string} key
   * @param {*} value
   * @param {string} collectionName
   * @return {(Promise<T | null>)}
   * @memberof Database
   */
  async fetchData<T>(key: string, value: any, collectionName: string): Promise<T | null> {
    if (value === undefined || value === null) {
      Core.Services.Logger.error(this.databaseType, `value passed in fetchData cannot be null or undefined`);
      return null;
    }

    if (!key || !collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify key, value, or collectionName for fetchAllByField.`);
      return null;
    }

    await this.hasInitialized();

    if (key === '_id' && typeof key !== 'object') {
      value = new ObjectId(value);
    }

    return await this.db.collection(collectionName).findOne<T>({ [key]: value });
  }

  /**
   * Fetch all data that matches a key and value pair as an array.
   * Use case: Fetching all users who have a specific boolean toggled.
   * @static
   * @template T
   * @param {string} key
   * @param {*} value
   * @param {string} collectionName
   * @return {Promise<T[]>}
   * @memberof Database
   */
  async fetchAllByField<T>(key: string, value: any, collectionName: string): Promise<T[]> {
    if (value === undefined || value === null) {
      Core.Services.Logger.error(this.databaseType, `value passed in fetchData cannot be null or undefined`);
      return null;
    }

    if (!key || !collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify key, value, or collectionName for fetchAllByField.`);
      return [];
    }

    await this.hasInitialized();

    if (key === '_id' && typeof key !== 'object') {
      value = new ObjectId(value);
    }

    const collection = await this.db.collection(collectionName);
    return await collection.find<T>({ [key]: value }).toArray();
  }

  /**
   * Get all elements from a collection.
   * @static
   * @template T
   * @param {string} collectionName
   * @return {Promise<Array<T[]>>}
   * @memberof Database
   */
  async fetchAllData<T>(collectionName: string): Promise<T[]> {
    if (!collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify collectionName for fetchAllData.`);
      return [];
    }

    await this.hasInitialized();

    const collection = await this.db.collection(collectionName);
    return collection.find<T>({}).toArray();
  }

  /**
   * Creates a search index for a specific 'text' field. Requires a 'string' field. Not numbers.
   * Use case: Searching for all users with 'Johnny' in their 'name' key.
   * @static
   * @template T
   * @param {string} key The key of the document that needs to be indexed
   * @param {string} collectionName The collection which this document needs indexing on.
   * @return {Promise<void>}
   * @memberof Database
   */
  async createSearchIndex(key: string, collectionName: string): Promise<void> {
    if (!collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify collectionName for createSearchIndex.`);
      return;
    }

    await this.hasInitialized();

    const collection = await this.db.collection(collectionName);
    const doesIndexExist = await collection.indexExists(key);

    if (!doesIndexExist) {
      await collection.createIndex({ [key]: 'text' });
    }
  }

  /**
   * Fetch all data that uses a search term inside a field name.
   * Use case: Searching for all users with 'Johnny' in their 'name' key.
   * @static
   * @template T
   * @param {string} searchTerm
   * @param {string} collectionName
   * @return {Promise<T[]>}
   * @memberof Database
   */
  async fetchWithSearch<T>(searchTerm: string, collectionName: string): Promise<T[]> {
    if (!collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify collectionName for fetchWithSearch.`);
      return [];
    }

    await this.hasInitialized();

    const collection = await this.db.collection(collectionName);
    let results;

    try {
      results = await collection.find<T>({ $text: { $search: searchTerm, $caseSensitive: false } }).toArray();
    } catch (err) {
      Core.Services.Logger.error(
        this.databaseType,
        `Failed to use 'createSearchIndex' before searching collection. Use 'createSearchIndex' function once, and property must be of stirng type in object.`
      );
      return [];
    }

    return results;
  }

  /**
   * Insert a document and return the new full document with _id.
   * Use case: Insert a new entry into the database.
   * @param {T} document
   * @param {string} collection
   * @param {boolean} returnDocument
   * @returns {Promise<T | null>} Document
   * @template T
   */
  async insertData<T>(document: T, collection: string, returnDocument = false): Promise<T> {
    if (!document || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify document or collection for insertData.`);
      return null;
    }

    await this.hasInitialized();

    const result = await this.db.collection(collection).insertOne(document);

    if (!returnDocument) {
      return null;
    }

    return await this.db.collection(collection).findOne<T>({ _id: result.insertedId });
  }

  /**
   * Modify an existing document in the database. Must have an _id first to modify data.
   * Use case: Update an existing document with new data, or update existing data.
   * @static
   * @param {*} _id
   * @param {Object} data
   * @param {string} collection
   * @param {Object} unset?
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async updatePartialData(_id: any, data: Object, collection: string, unset?: Object): Promise<boolean> {
    if (!_id || !data || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify id, data or collection for updatePartialData.`);
      return null;
    }

    await this.hasInitialized();

    if (typeof _id !== 'object') {
      _id = new ObjectId(_id);
    }

    try {
      if (unset) {
        await this.db.collection(collection).findOneAndUpdate({ _id }, { $set: { ...data }, $unset: { ...unset } });
      } else {
        await this.db.collection(collection).findOneAndUpdate({ _id }, { $set: { ...data } });
      }

      return true;
    } catch (err) {
      Core.Services.Logger.error(this.databaseType, `Could not find and update a value with id: ${_id.toString()}`);
      return false;
    }
  }

  /**
   * Modify an existing document in the database using raw mongodb syntax. Must have an _id first to modify data.
   * Use case: Update an existing document with specific update operators
   * @static
   * @param {*} _id
   * @param {Object} rawData
   * @param {string} collection
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async updatePartialDataRaw(_id: any, rawData: Object, collection: string): Promise<boolean> {
    if (!_id || !rawData || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify id, data or collection for updatePartialDataRaw.`);
      return null;
    }

    await this.hasInitialized();

    if (typeof _id !== 'object') {
      _id = new ObjectId(_id);
    }

    try {
      await this.db.collection(collection).findOneAndUpdate({ _id }, rawData);
      return true;
    } catch (err) {
      Core.Services.Logger.error(this.databaseType, `Could not find and update a value with id: ${_id.toString()}`);
      return false;
    }
  }

  /**
   * Removes an existing field from an document. Must have an _id first to remove fields.
   * Use case: Update existing document with new data structure
   * @static
   * @param {*} _id
   * @param {Object} data
   * @param {string} collection
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async removePartialData(_id: any, data: Object, collection: string): Promise<boolean> {
    if (!_id || !data || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify id, data or collection for removePartialData.`);
      return null;
    }

    await this.hasInitialized();

    if (typeof _id !== 'object') {
      _id = new ObjectId(_id);
    }

    try {
      await this.db.collection(collection).findOneAndUpdate({ _id }, { $unset: { ...data } });
      return true;
    } catch (err) {
      Core.Services.Logger.error(this.databaseType, `Could not find and update a value with id: ${_id.toString()}`);
      return false;
    }
  }

  /**
   * Delete a document by _id and collection.
   * Use case: Delete the entry from the database collection.
   * @static
   * @param {*} _id
   * @param {string} collection
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async deleteById(_id: any, collection: string): Promise<boolean> {
    if (!_id || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify id, or collection for deleteById`);
      return false;
    }

    await this.hasInitialized();

    if (typeof _id !== 'object') {
      _id = new ObjectId(_id);
    }

    try {
      await this.db.collection(collection).findOneAndDelete({ _id });
      return true;
    } catch (err) {
      return false;
    }
  }

  /**
   * Specify a list of fields to select from the database in a collection.
   * Use case: Selects all data from a collection and only returns the specified keys.
   * @template T
   * @param {string} collection
   * @param {string[]} keys
   * @return {Promise<T[]>}
   * @memberof Database
   */
  async selectData<T>(collection: string, keys: string[]): Promise<T[]> {
    if (!keys || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify keys, or collection for selectData`);
      return [];
    }

    await this.hasInitialized();

    const selectData = {
      _id: 1
    };

    for (let i: number = 0; i < keys.length; i++) {
      selectData[keys[i]] = 1;
    }

    return await this.db
      .collection(collection)
      .find<T>({})
      .project<T>({ ...selectData })
      .toArray();
  }

  /**
   * Uses default mongodb element match functionality.
   *
   * See: https://www.mongodb.com/docs/manual/reference/operator/query/elemMatch/#array-of-embedded-documents
   *
   * @param {string} collection
   * @param {string} propertyName
   * @param {{ [key: string]: any }} elementMatch
   * @returns
   */
  async selectWithElementMatch<T>(collection: string, propertyName: string, elementMatch: { [key: string]: any }): Promise<T[]> {
    if (!collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify keys, or collection for selectData`);
      return [];
    }

    await this.hasInitialized();

    const currentCollection = await this.db.collection(collection);

    return currentCollection.find<T>({ [propertyName]: { $elemMatch: elementMatch } }).toArray();
  }

  /**
   * Update any data that matches specified field name and value.
   * Use case: Could be used to migrate old field values to new field values in bulk in a collection.
   * @param {string} key
   * @param {*} value
   * @param {Object} data
   * @param {string} collection
   * @return {*}  {Promise<boolean>}
   * @memberof Database
   */
  async updateDataByFieldMatch(key: string, value: any, data: Object, collection: string): Promise<boolean> {
    if (value === undefined || value === null) {
      Core.Services.Logger.error(this.databaseType, `value passed in fetchData cannot be null or undefined`);
      return null;
    }

    if (!key || !data || !collection) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify key, value, data, or collection for updateDataByFieldMatch.`);
      return false;
    }

    await this.hasInitialized();

    if (key === '_id' && typeof value !== 'object') {
      value = new ObjectId(value);
    }

    const updated = await this.db.collection(collection).findOneAndUpdate({ [key]: value }, { $set: { ...data } });

    return !(!updated || !updated.ok);


  }

  /**
   * Drop a collection from the database.
   * @static
   * @param {string} collectionName
   * @return {Promise<void>}
   * @memberof Database
   */
  async dropCollection(collectionName: string): Promise<boolean> {
    if (!collectionName) {
      Core.Services.Logger.error(this.databaseType, `Failed to specify collectionName for dropCollection.`);
      return false;
    }

    await this.hasInitialized();

    let res = false;

    try {
      res = await this.db
        .collection(collectionName)
        .drop()
        .then((res) => {
          return true;
        })
        .catch((err) => {
          return false;
        });
    } catch (err) {
      Core.Services.Logger.error(this.databaseType, `Did not find ${collectionName} to drop.`);
    }

    return res;
  }

  /**
   * Remove an entire database from MongoDB. Including all collections.
   * @static
   * @return {Promise<boolean>}
   * @memberof Database
   */
  async dropDatabase(): Promise<boolean> {
    await this.hasInitialized();

    return await this.client
    .db()
    .dropDatabase()
    .catch((err) => {
      Core.Services.Logger.error(this.databaseType, err);
      return false;
    })
    .then((res) => {
      Core.Services.Logger.database(this.databaseType, `Dropped database successfully.`);
      return true;
    });
  }

  /**
   * Close the connection to the database.
   * @static
   * @return {Promise<void>}
   * @memberof Database
   */
  async close(): Promise<void> {
    if (!this.client) {
      this.db = null;
      this.isInitialized = false;
      return;
    }

    await this.client.close(true);

    this.client = null;
    this.db = null;
    this.isInitialized = false;
  }
}

export const MongoDB: MongoDBClass = new MongoDBClass();