import orm from 'typeorm';
import { Core } from '../../startup.js';

let currentConnection = undefined;

export class MySQL {
  connected: boolean = false;
  connection = undefined;
  databaseType: string = 'MySQL';

  constructor(array: any, dbConfig: Object, sync: boolean) {
    try {
      if (!currentConnection) {
        const config = {
          type: `${dbConfig['type']}`,
          host: `${dbConfig['host']}`,
          port: `${dbConfig['port']}`,
          username: `${dbConfig['username']}`,
          password: `${dbConfig['password']}`,
          database: `${dbConfig['database']}`,
          entities: array,
          cache: true
        };

        //@ts-ignore
        orm.createConnection(config) // Generate Connection
          .then(conn => { // If Successful ...
            this.connection = conn; // Set Internal Variable "connection", so we can use it in other Functions of this Class
            if (array && array.length > 0 && sync) {
              conn.synchronize() // Syncronizize Database with Entities
                .then(() => { // If Successful ..
                  currentConnection = this; // Set Global Connection, so this Sync and Connection only starts one Time
                  this.connected = true;
                  Core.Services.Logger.database(this.databaseType,`Database Connection successful [ Host: '${config.host}' | DB: '${config.database}' ]`);
                  return currentConnection; // returns Global Connection
                })
            } else {
              currentConnection = this; // Set Global Connection, so this Sync and Connection only starts one Time
              this.connected = true;
              Core.Services.Logger.database(this.databaseType,`Database Connection successful [ Host: '${config.host}' | DB: '${config.database}' ]`);
              return currentConnection; // returns Global Connection
            }
          })
          .catch(err => {
            Core.Services.Logger.database(this.databaseType,`${err}`);
            setTimeout(() => {
              process.exit(1);
            }, 2000);
          }) // if Error, show Error and Exit Program
      }
      return currentConnection; // returns Global Connection
    } catch (error) {
      Core.Services.Logger.database(this.databaseType,`${error}`);
    }
  }

  async fetchDataAsync(repoName: string, document: any): Promise<object> {
    return new Promise((resolve, reject) => {
      const repo = this.connection.getRepository(repoName);
      repo.find({ where: document })
        .then(res => {
          return resolve(res);
        })
        .catch(err => {
          return reject(err);
        })
    })
  }

  async fetchAllDataAsync(repoName: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const repo = this.connection.getRepository(repoName);
      repo.find()
        .then(res => {
          return resolve(res);
        })
        .catch(err => {
          return reject(err);
        })
    })
  }

  /**
   * Async | Delete the Entrys of Table with the specific primary ID
   * @param {string} repoName Tablename
   * @param {array | int} ids Array of ID´s that should find
   * @returns {*}
   * @example Core.Database.deleteByIdsAsync('account', [ids], (err, res) => {})
   */
  async deleteByIdsAsync(repoName: string, ids: [number]): Promise<any> {
    return new Promise((resolve, reject) => {
      const repo = this.connection.getRepository(repoName);
      let idRef = ids;
      if (!Array.isArray(ids)) {
        idRef = [ids];
      }

      repo.delete(idRef)
        .then(res => {
          return resolve(res);
        })
        .catch(err => {
          return reject(err);
        })
    })
  }

  /**
   * Async | Insert or Update DB Entry | Ident is Primary Key, 0 = Insert | ~ = Update (if exists)
   * @param {string} repoName Name of Entity-Repository
   * @param {object} record Array of DB:Columns and Values ( example: { id: 0, name: 'myName' } )
   * @example Core.Database.upsertDataAsync('account', {name: Jack}, (err, res) => {});
   */
  async upsertDataAsync(repoName: string, record: object): Promise<any> {
    return new Promise((resolve, reject) => {
      const repo = this.connection.getRepository(repoName);
      repo.save(record)
        .then(res => {
          return resolve(res);
        })
        .catch(err => {
          return reject(err);
        })
    });
  }
}
