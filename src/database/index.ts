import { MySQL } from "./mysql/mysql.js";
import { MongoDB } from "./mongodb/mongodb.js";

class DatabaseClass {
  public MySQL = MySQL;
  public MongoDB = MongoDB;
}

export const Database: DatabaseClass = new DatabaseClass();