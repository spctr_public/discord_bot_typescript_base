import { Core } from './startup.js';
import { glob } from "glob";
import * as fs from 'fs';
import YAML from 'yaml';

/**
 * import all event files out of the event folders of the given path
 * ! you can use wildcards
 */
export class System {

  static async importEvents(path: string): Promise<void> {
    try {
      const eventPath: string[] = await glob(`${process.cwd()}/dist/${path}`)

      let filePath: any[] = [];
      for (const path of eventPath) {
        filePath.push(path.split('dist\\')[1])
      }

      if (eventPath) {
        for (let x: number = 0; x < filePath.length; x++) {
          const files: string[] = fs.readdirSync(`${process.cwd()}/dist/${filePath[x]}`)
          for (let i: number = 0; i < files.length; i++) {
            import(`./${filePath[x]}/${files[i]}`)
          }
        }
      }
    } catch {
      console.log('\x1b[31m', '[System]', '\x1b[0m', `no 'events' folder in '${path}'`)
    }
  }

  static checkForFile(file: string): void {
    try {
      if (fs.existsSync(file)) {

        const config = fs.readFileSync(file, 'utf-8');
        const yml = YAML.parse(config);

        /**
         * import all files from all given event folders
         */
        this.importEvents('modules/**/events');
        this.importEvents('plugins/**/events');

        Core.start(yml['Bot']['database'], yml['Bot']['token'], yml['database'], yml).catch(err => {
          throw err;
        });
      } else {
        console.log('\x1b[31m', '[Startup]', '\x1b[0m', `The file '${file}' does not exist but is needed.`)
      }
    } catch (error) {
      console.log('\x1b[31m', '[System]', '\x1b[0m', `Startup: ${error}`)
    }
  }
}

/**
 * check if file(s) exist on startup
 */
System.checkForFile('./config.yml');