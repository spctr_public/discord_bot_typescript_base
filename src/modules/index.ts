import { Discord } from "./discord/index.js";
import {Core} from "../startup.js";

class ModuleClass {

  public Discord = Discord;

  public init(): void {
    Core.Modules.Discord.init();
  }
}

export const Modules: ModuleClass = new ModuleClass();