import { Core } from "../../../startup.js";
import { ButtonStyle, Channel, Events, Interaction, User } from "discord.js";


Core.client.on(Events.InteractionCreate, async (interaction: Interaction): Promise<void> => {
  try {

    //! variables
    const user: User = interaction.user;

    if (interaction.isCommand()) {
      switch (interaction.commandName) {
        case 'ping':
          await interaction.reply({
            content: `🏓 Latency: ${Core.client.ws.ping}ms`
          });
          break;
        default:
          break;
      }
    }

    if (interaction.isModalSubmit()) {}

    if (interaction.isButton()) {}

  } catch (error) {
    Core.Services.Logger.error('InteractionCreate', error);
  }
});