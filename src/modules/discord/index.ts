import { Core } from "../../startup.js";
import * as fs from "fs";
import YAML from 'yaml';
import { Command } from "./scripts/command.js";

class DiscordModuleClass {
  private moduleName: string = 'Discord';
  public Command = Command;

  public init(): void {
    try {
      Core.Services.Logger.moduleImport(this.moduleName);
      Core.Modules.Discord.Command.init();
    } catch (error) {
      Core.Services.Logger.error(this.moduleName, error);
    }
  }

  public getConfig(): any {
    try {
      const config = fs.readFileSync('./config/discord.yml', 'utf-8');
      return YAML.parse(config);
    } catch (error) {
      Core.Services.Logger.error(this.moduleName, error);
    }
  }
}

export const Discord: DiscordModuleClass = new DiscordModuleClass();