import { Core } from "../../../startup.js";
import { REST, SlashCommandBuilder } from "discord.js";
import { Routes } from 'discord-api-types/v10';

class DiscordCommand {
  private moduleName: string = 'Discord Command';

  public init(): void {
    try {
      this.setRest();
    } catch (error) {
      Core.Services.Logger.error(this.moduleName, `${error}`);
    }
  }

  private commandMapping(): object {
    try {
      const config = Core.Modules.Discord.getConfig();
      let array: Array<any> = [];
      for (let i: number = 0; i < config['Commands'].length; i++) {
        array.push(new SlashCommandBuilder().setName(config['Commands'][i].name).setDescription(config['Commands'][i].description));
      }
      return array.map((command) => command.toJSON());

    } catch (error) {
      Core.Services.Logger.error(this.moduleName, error);
      return undefined;
    }
  }

  public setRest(): void {
    try {
      const rest: REST = new REST({ version: '10'}).setToken(Core.Config['Bot']['token']);
      rest.put(Routes.applicationGuildCommands(Core.Config['Bot']['client_id'], Core.Config['Bot']['guild_id']), { body: this.commandMapping() })
        .then(() => Core.Services.Logger.log(this.moduleName, 'Application commands were initialized'))
        .catch(console.error);
    } catch (error) {
      Core.Services.Logger.error(this.moduleName, error);
    }
  }
}

export const Command: DiscordCommand = new DiscordCommand();