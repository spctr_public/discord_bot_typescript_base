import { Embed } from "./embed.service.js";
import { Core } from "../../../startup.js";

class DiscordServiceClass {
  private serviceName: string = 'Discord';
  public Embed = Embed;

  public init(): void {
    try {
      Core.Services.Logger.serviceImport(this.serviceName);
    } catch (error) {
      Core.Services.Logger.error(this.serviceName, error);
    }
  }
}

export const Discord: DiscordServiceClass = new DiscordServiceClass();