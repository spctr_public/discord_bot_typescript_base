import { Core } from "../../../startup.js";
import * as discord from "discord.js";
import { EmbedCreator, FormsCreator, ButtonCreator, SelectMenuCreator } from "./interfaces/embed.interface.js"

class EmbedService {
  private ServiceName: string = 'Embed';

  public init(): void {
    Core.Services.Logger.serviceImport(this.ServiceName);
  }

  /**
   * this function is simple to use and creates a full embed
   * @param data
   * @returns EmbedBuilder
   */
  public create(data?: EmbedCreator): discord.EmbedBuilder {
    try {
      const embed: discord.EmbedBuilder = new discord.EmbedBuilder()
        .setColor(data.color)
        .setTitle(data.title)

      if (data.user) embed.setAuthor({name: data.user.username, iconURL: data.user.displayAvatarURL()});
      if (data.desc && data.codeBlock) {
        embed.setDescription(discord.codeBlock(data.desc));
      } else if (data.desc) {
        embed.setDescription(data.desc);
      }

      if (data.footer) embed.setFooter({text: data.footer, iconURL: 'https://i.ibb.co/z4f60tz/3d-Logo-Transparent-600x600.png'});

      if (data.field) {
        for (let i: number = 0; i < data.field.length; i++) {
          embed.addFields({ name: data.field[i].name, value: data.field[i].value, inline: true });
        }
      }

      return embed
    } catch (error) {
      Core.Services.Logger.error(this.ServiceName, `${error}`);
      return undefined;
    }
  }

  /**
   * create a dropdown menu for the user
   * @param {SelectMenuCreator} data
   * @returns a custom menu
   */
  public addSelectMenu(data?: SelectMenuCreator): any {
    try {
      const menu: discord.StringSelectMenuBuilder = new discord.StringSelectMenuBuilder()
        .setCustomId(data.customId)
        .setPlaceholder(data.placeholder)

      for (let i: number = 0; i < data.options.length; i++) {
        menu.addOptions({
          label: `${data.options[i].label}`,
          description: `${data.options[i].description}`,
          value: `${data.options[i].value}`
        })
      }

      return new discord.ActionRowBuilder().addComponents(menu);
    } catch (error) {
      Core.Services.Logger.error(this.ServiceName, `${error}`);
      return undefined;
    }
  }

  /**
   *
   * @returns buttons
   * @param {ButtonCreator} data
   */
  public addButton(data?: ButtonCreator): any {
    try {
      let row: discord.ActionRowBuilder<discord.AnyComponentBuilder> = new discord.ActionRowBuilder();

      for (let i: number = 0; i < data.btn.length; i++) {
        const button: discord.ButtonBuilder = new discord.ButtonBuilder()
          .setLabel(data.btn[i].label)
          .setStyle(data.btn[i].style)

        if (data.btn[i].style === discord.ButtonStyle.Link) {
          if (!data.btn[i].url) {
            Core.Services.Logger.error(this.ServiceName, 'no url in button');
            return null;
          }
          button.setURL(data.btn[i].url);
        }
        button.setCustomId(data.btn[i].customId)

        if (data.btn[i].disabled) {
          button.setDisabled(true)
        }

        row.addComponents(button);
      }
      return row;
    } catch (error) {
      Core.Services.Logger.error(this.ServiceName, `${error}`);
      return undefined;
    }
  }

  /**
   * create a form
   * @param data?
   */
  public addForms(data?: FormsCreator): any {
    try {
      let modal: discord.ModalBuilder = new discord.ModalBuilder()
        .setCustomId(data.modalCustomId)
        .setTitle(data.title)

      const inputStyle = {
        'paragraph': discord.TextInputStyle.Paragraph,
        'short': discord.TextInputStyle.Short,
      }

      for (let i: number = 0; i < data.input.length; i++) {
        const field: discord.TextInputBuilder = new discord.TextInputBuilder()
          .setCustomId(data.input[i].customId)
          .setLabel(data.input[i].inputLabel)
          .setStyle(inputStyle[data.input[i].inputStyle.toLowerCase()])

        if (data.input[i].placeholder !== undefined) field.setPlaceholder(data.input[i].placeholder);
        if (data.input[i].required !== undefined) field.setRequired(data.input[i].required);
        if (data.input[i].max !== undefined) field.setMinLength(data.input[i].max);
        if (data.input[i].min !== undefined) field.setMaxLength(data.input[i].min);

        const builder: discord.ActionRowBuilder<any> = new discord.ActionRowBuilder().addComponents(field);
        modal.addComponents(builder);
      }
      return modal;
    } catch (error) {
      Core.Services.Logger.error(this.ServiceName, `${error}`);
      return undefined;
    }
  }
}

export const Embed: EmbedService = new EmbedService();