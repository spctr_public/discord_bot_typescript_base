import { ButtonStyle, ColorResolvable, User } from "discord.js";

export interface EmbedCreator {
  title?: string | null;
  desc?: string | null;
  codeBlock?: boolean | null;
  color?: ColorResolvable | null;
  user?: User | null;
  footer?: string | null;
  field?: Array<{ name: string, value: string }> | null;
}

export interface FormsCreator {
  modalCustomId?: string | null,
  title?: string | null,
  input?: Array<{ customId: string, inputLabel: string, inputStyle: string, placeholder?: string, required?: boolean, max?: number, min?: number }> | null
}

export interface SelectMenuCreator {
  customId?: string | null,
  placeholder?: string | null,
  options?: Array<{ label: string, description: string, value: string }> | null
}

export interface ButtonCreator {
  btn: Array<{customId: string, label: string, style: ButtonStyle, url?: string, disabled?: boolean}> | null
}
