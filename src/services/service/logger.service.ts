
class LoggerService {

  private colors = {
    reset: "\x1b[0m",
    blink: "\x1b[5m",

    black: "\x1b[30m",
    red: "\x1b[31m",
    green: "\x1b[32m",
    yellow: "\x1b[33m",
    blue: "\x1b[34m",
    magenta: "\x1b[35m",
    cyan: "\x1b[36m",
    white: "\x1b[37m",
  }

  public serviceName: string = 'Logger';
  public init(): void {
    try {
      this.serviceImport(this.serviceName);
    } catch (error) {
      this.error(this.serviceName, error);
    }
  }

  public log(location: string, message: string): void {
    console.log(this.colors.cyan, '[ Log ]', this.colors.reset, `${location} : ${message}`);
  }

  public error(location: string, message: string): void {
    console.log(this.colors.red, '[ Error ]', this.colors.reset, `${location} : ${message}`);
  }

  public database(location: string, message: string): void {
    console.log(this.colors.magenta, '[ Database ]', this.colors.reset, `${location} : ${message}`);
  }

  public debug(location: string, message: string): void {
    console.log(this.colors.yellow, '[ Debug ]', this.colors.reset, `${location} : ${message}`);
  }

  public moduleImport(module: string): void {
    console.log(this.colors.green, '[ Module Import ]', this.colors.reset, `loaded: ${module}`);
  }

  public serviceImport(service: string): void {
    console.log(this.colors.green, '[ Service Import ]', this.colors.reset, `loaded: ${service}`);
  }
}

export const Logger: LoggerService = new LoggerService();