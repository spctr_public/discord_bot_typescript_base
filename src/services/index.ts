import { Logger } from "./service/logger.service.js";
import { Core } from "../startup.js";
import { Discord } from "./service/discord/index.js";

class ServiceClass {

  public Logger = Logger;
  public Discord = Discord;

  public init(): void {
    Core.Services.Logger.init();
    Core.Services.Discord.init();
  }
}

export const Services: ServiceClass = new ServiceClass();