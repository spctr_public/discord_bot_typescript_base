import { Client, Events, GatewayIntentBits, ActivityType } from "discord.js";
import { Modules } from "./modules/index.js";
import { Services } from "./services/index.js";
import { Database } from "./database/index.js";

let Core: CoreClass;

class CoreClass {

  public Modules = Modules;
  public Services = Services;
  public EntityArray: any[] = [];
  public Database = Database;
  public Config: any;

  /**
   * create your bot client and add/remove all needed Intents
   */
  public client: Client<boolean> = new Client({
    intents: [
      GatewayIntentBits.Guilds,
      GatewayIntentBits.GuildMessages,
      GatewayIntentBits.MessageContent,
      GatewayIntentBits.GuildMembers,
      GatewayIntentBits.DirectMessages,
      GatewayIntentBits.GuildVoiceStates,
      GatewayIntentBits.GuildPresences,
    ]
  });

  public async start(databaseType: string, token: string, dbConfig: Object, botConfig: Object): Promise<void> {
    try {

      /**
       * handle client login error
       */
      if (!this.client) {
        console.log('\x1b[31m', '[System]', '\x1b[0m', 'System was unable to start the client');
        return;
      }

      this.Config = botConfig;

      /**
       * initialize all needed modules and services of the system
       */
      console.log('\n\x1b[31m', '[System]', 'initialize services ==========');
      Core.Services.init();

      console.log('\n\x1b[31m', '[System]', 'initialize modules ==========');
      Core.Modules.init();

      /**
       * login the client and start the bot
       */
      await this.client.login(token);
      this.client.once(Events.ClientReady, bot => {
        bot.user.setActivity(
          'discord.js boilerplate',
          { type: ActivityType.Playing }
        );
        console.log('\n\x1b[36m', '[System]', '\x1b[0m', `System has started the client '${bot.user.tag}'`);
        console.log('\x1b[36m', '[System]', '\x1b[0m', `Status: '${bot.user.presence.status}'`);
      })

      switch (databaseType.toLowerCase()) {
        case 'mysql':
          this.Database.MySQL.prototype = new this.Database.MySQL(this.EntityArray, dbConfig['database'], true)
          break;
        case 'mongodb':
          this.Database.MongoDB = new this.Database.MongoDB.init(dbConfig['database']['url'], dbConfig['database']['name'], this.EntityArray);
          break;
        default:
          console.log('\x1b[31m', '[System]', '\x1b[0m', `Startup: This type of database is not supported!`);
          break;
      }

    } catch (error) {
      console.log('\x1b[31m', '[System]', '\x1b[0m', `Startup: ${error}`)
    }
  }
}

/**
 * set CoreClass into Core variable and export it
 * to use Core in your project
 */
Core = new CoreClass();
export { Core }